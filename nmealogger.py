# import serial
# import time

# port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=0)


# def yieldlines(port):
#     line = b''
#     while True:
#         byte = port.read()
#         if byte == b'\r':
#             pass
#         elif byte == b'\n':
#             yield line
#             line = b''
#         else:
#             line += byte  # .decode('utf-8')


# for line in yieldlines(port):
#     print(line, flush=True)

import serial
with serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout=1) as ser:
    # read 10 lines from the serial output
    for i in range(10):
        line = ser.readline()  # .decode('ascii', errors='replace')
        print(line.strip())
