#!/usr/bin/python
# -*- coding: utf-8 -*-


# import httplib
# import urllib
import random
import time
import serial
import logging


def readlineCR(port):
    rv = ''
    count = 0
    while True:
        ch = port.read()
        rv += ch.decode('utf-8')

        if ch == b'\r':
            count = count + 1
            print(count)
        elif ch == b'':
            pass
        else:
            print(ch)

        if count == 14:
            return rv

port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=0)
#terminal_modem =serial.Serial("/dev/ttyUSB0",baudrate=9600, timeout=0)

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s',
                    filename='./weather_station.log', filemode='w')


while True:
    rcv = readlineCR(port)
    print(rcv, flush=True)
    data = rcv.split('\r\n')
    print(data, flush=True)
    i = 0
    weather_data = {}
    date_time = time.strftime("%Y-%m-%dT%H:%M:%SZ")
    for i in range(len(data)):

        if data[i].split("=")[0] == "WIND SPEED MAX":
            windspd_kmh_max = round(
                ((float(data[i].split("=")[1]) / 3) * 3.6215), 2)
            weather_data.update({'windspd_kmh': windspd_kmh_max})

        if data[i].split("=")[0] == "RAIN COUNTER":
            rain_mm = ((float(data[i].split("=")[1])) * 0.2)
            weather_data.update({'rain_mm': rain_mm})
            weather_data.update({'date_time': date_time})

        if data[i].split("=")[0] == "WIND DIRECTION":
            winddir = data[i].split("=")[1]
            weather_data.update({'winddir': winddir})

        if data[i].split("=")[0] == "BAROMETER VALUE":
            pressure = (
                ((0.0012207 * float(data[i].split("=")[1])) / 5 + 0.095) / 0.0009)
            weather_data.update({'pressure': round(pressure, 2)})

        if data[i].split("=")[0] == "TEMPERATURE VALUE":
            temp_c = -40 + (0.01 * float(data[i].split("=")[1]))
            weather_data.update({'temp_c': round(temp_c, 2)})

        if data[i].split("=")[0] == "HUMIDITY VALUE":
            linear_humidity = (
                (0.0405 + (-2.8e-6 * float(data[i].split("=")[1]))) * float(data[i].split("=")[1])) + -4
            rh_pct = (temp_c - 25) * (0.01 + 0.00008 *
                                      float(data[i].split("=")[1])) + linear_humidity
            weather_data.update({'rh_pct': round(rh_pct, 2)})

    # params = urllib.urlencode(weather_data)
    logging.debug(weather_data)
    # data = urllib.urlopen("http://localhost:500/rest/data/",params)
    # logging.info(data.read())
    # logging.debug("Sent Weather")
    # logging.debug(weather_data)
    # time.sleep(900)
