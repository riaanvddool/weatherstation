from gps import *
import logging
import threading
import time


def round_or_none(value, ndigits):
    return None if value is None else round(value, ndigits)


def gpsloop(name):
    logging.info('Thread {}: starting'.format(name))
    try:
        gpsd = gps(port=2948, mode=WATCH_ENABLE | WATCH_NEWSTYLE)
        while True:
            report = gpsd.next()
            logging.debug('Thread {}: report class: {}'.format(
                name, report['class']))
            if report['class'] == 'TPV':
                tpv = {}
                tpv['lat'] = round_or_none(report.get('lat'), 4)
                tpv['lon'] = round_or_none(report.get('lon'), 4)
                tpv['time'] = report.get('time', '')
                tpv['alt'] = round_or_none(report.get('alt'), 1)
                tpv['track'] = round_or_none(report.get('track'), 1)
                tpv['speed'] = round_or_none(report.get('speed'), 1)
                tpv['climb'] = round_or_none(report.get('climb'), 1)
                tpv['epv'] = round_or_none(report.get('epv'), 1)
                tpv['ept'] = round_or_none(report.get('ept'), 1)
                tpv['epd'] = round_or_none(report.get('epd'), 1)
                tpv['eps'] = round_or_none(report.get('eps'), 1)
                tpv['epx'] = round_or_none(report.get('epx'), 1)
                tpv['epy'] = round_or_none(report.get('epy'), 1)
                logging.info('Thread {}: TPV {}'.format(name, tpv))
            elif report['class'] == 'SKY':
                satellites = report.get('satellites', [])
                sats_used = 0
                sats_total = len(satellites)
                for sat in satellites:
                    if getattr(sat, 'used') is True:
                        sats_used += 1
                logging.info(
                    'Thread {}: Satellites used: {}/{}'.format(name, sats_used, sats_total))

            time.sleep(1)

    except Exception as e:  # when you press ctrl+c
        errno, strerror = e.args
        logging.error('Thread {}: caught exception ({}) {}'.format(
            name, errno, strerror))
    finally:
        logging.info('Thread {}: finishing'.format(name))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Test GPS manager.')
    parser.add_argument('-p', '--port', type=int, default=2947,
                        help='Port used for connecting to gpsd')
    args = parser.parse_args()
    print(args.port)

    format = '%(asctime)s: %(message)s'
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt='%H:%M:%S')

    logging.info('Main    : before creating thread')
    thread = threading.Thread(target=gpsloop,
                              daemon=True, args=(args.port,))
    logging.info('Main    : before running thread')
    thread.start()
    logging.info('Main    : wait for the thread to finish')
    thread.join()
    logging.info('Main    : all done')
