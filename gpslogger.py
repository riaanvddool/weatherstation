from gps import *
import time

gpsd = gps(port=2948, mode=WATCH_ENABLE | WATCH_NEWSTYLE)


def round_or_none(value, ndigits):
    return None if value is None else round(value, ndigits)

try:
    while True:
        report = gpsd.next()
        if report['class'] == 'TPV':
            tpv = {}
            tpv['lat'] = round_or_none(report.get('lat'), 4)
            tpv['lon'] = round_or_none(report.get('lon'), 4)
            tpv['time'] = report.get('time', '')
            tpv['alt'] = round_or_none(report.get('alt'), 1)
            tpv['track'] = round_or_none(report.get('track'), 1)
            tpv['speed'] = round_or_none(report.get('speed'), 1)
            tpv['climb'] = round_or_none(report.get('climb'), 1)
            tpv['epv'] = round_or_none(report.get('epv'), 1)
            tpv['ept'] = round_or_none(report.get('ept'), 1)
            tpv['epd'] = round_or_none(report.get('epd'), 1)
            tpv['eps'] = round_or_none(report.get('eps'), 1)
            tpv['epx'] = round_or_none(report.get('epx'), 1)
            tpv['epy'] = round_or_none(report.get('epy'), 1)
            print(tpv)
        elif report['class'] == 'SKY':
            satellites = report.get('satellites', [])
            sats_used = 0
            sats_total = len(satellites)
            for sat in satellites:
                if getattr(sat, 'used') is True:
                    sats_used += 1
            print('Satellites used: {}/{}'.format(sats_used, sats_total))

        time.sleep(1)

except (KeyboardInterrupt, SystemExit):  # when you press ctrl+c
    print("Done.\nExiting.")
