import serial
import time

port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=0)


def yieldlines(port):
    line = ''
    while True:
        byte = port.read()
        if byte == b'\r':
            pass
        elif byte == b'\n':
            yield line
            line = ''
        else:
            line += byte.decode('utf-8')


# Convert to a relative degrees value because the actual wind direction is
# dependant on the installation of the anemometor
def wind_dir_to_deg(dir):
    step = {
        'N': 0,
        'NNE': 1,
        'NE': 2,
        'ENE': 3,
        'E': 4,
        'ESE': 5,
        'SE': 6,
        'SSE': 7,
        'S': 8,
        'SSW': 9,
        'SW': 10,
        'WSW': 11,
        'W': 12,
        'WNW': 13,
        'NW': 14,
        'NNW': 15,
    }.get(dir)
    return None if step is None else step * 22.5


def field_name_and_value(key, value, data={}):
    if key == "WIND SPEED MAX":
        field_name = 'wmax'
        field_value = round((float(value) / 3) * 3.6215, 1)
    elif key == "WIND SPEED MIN":
        field_name = 'wmin'
        field_value = round((float(value) / 3) * 3.6215, 1)
    elif key == "WIND AVERAGE":
        field_name = 'wavg'
        field_value = round((float(value) / 3) * 3.6215, 1)
    elif key == "WIND COUNTER":
        field_name = 'wc'
        field_value = round(float(value), 0)
    elif key == "RAIN COUNTER":
        field_name = 'rain'
        field_value = round(float(value) * 0.2, 1)
    elif key == "WIND DIRECTION":
        field_name = 'wdir'
        field_value = wind_dir_to_deg(value)
    elif key == "BAROMETER VALUE":
        field_name = 'pressure'
        field_value = round(
            ((0.0012207 * float(value)) / 5 + 0.095) / 0.0009)
    elif key == "TEMPERATURE VALUE":
        field_name = 'temp'
        field_value = round(-40 + (0.01 * float(value)), 1)
    elif key == "HUMIDITY VALUE":
        field_name = 'rh'
        temp = data.get('temp')
        if temp:
            linear_humidity = (
                (0.0405 + (-2.8e-6 * float(value))) * float(value)) - 4
            field_value = round(
                (temp - 25) * (0.01 + 0.00008 * float(value)) + linear_humidity)
        else:
            field_value = None
    else:
        field_name = key.lower().replace(' ', '_')
        field_value = value

    return field_name, field_value


def send(data):
    packet = '{date_time},{wmax:g},{wmin:g},{wavg:g},{wdir:g},{pressure:g},{temp:g},{rh:g},{rain:g}'.format(
        **data)
    print(packet, flush=True)


weather_log = []
data = None


for line in yieldlines(port):
    print(line, flush=True)
    if line == '':
        if data:
            print(data)
            send(data)
        data = None
        continue
    key, value = line.split("=")
    if key in ['MODEL', 'INTERVAL SECS']:
        date_time = time.strftime('%Y%m%d%H%M%S')
        data = {'date_time': date_time}
    elif data is not None:
        field_name, field_value = field_name_and_value(key, value, data)
        # print(field_name, field_value)
        data.update({field_name: field_value})
    else:
        print('no data')
